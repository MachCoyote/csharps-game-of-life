﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using System.Windows;
using SFML;
using SFML.Graphics;
using SFML.Window;
using SFML.Audio;
using SFML.System;

//ORDER FOR COORDS IS Y-X - YEAH ITS DUMB

namespace CSharps_Game_Of_Life
{
    class Program
    {
        public int rows;
        public int cols;
        public int rowLength;
        public int colLength;
        public int[,] boardSize;
        public int[,] dummyBoard;
        public int[,] displayBoard;
        public int generationNum = 0;
        private bool consoleUI;
        private int surroundingPop;
        private System.Timers.Timer generation;
        

        static void Main(string[] args)
        {
            Program software = new Program();
            software.initAndDisplay();
        }

        public void initAndDisplay()
        {
            //get user input for rows and cols and timer and stores in variables
            Console.Write("Enter amount of rows: ");
            string userInput = Console.ReadLine();
            rows = int.Parse(userInput);
            Console.WriteLine();
            Console.Write("Enter amount of columns: ");
            userInput = Console.ReadLine();
            cols = int.Parse(userInput);
            Console.WriteLine();
            Console.Write("Enter interval between generations in milliseconds: ");
            userInput = Console.ReadLine();
            int interval = int.Parse(userInput);
            Console.WriteLine();
            Console.Write("Use CLI UI? (THIS WILL CAUSE LAG) (y/n): ");
            userInput = Console.ReadLine();
            if (userInput == "y")
            {
                consoleUI = true;
            }
            else if (userInput == "n")
            {
                consoleUI = false;
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Error: must enter y or n, defaulting to n.");
                consoleUI = false;
            }
            

            boardSize = new int[rows, cols];
            dummyBoard = new int[rows, cols];
            displayBoard = new int[rows, cols];

            testclass1 testing = new testclass1();
            testing.testInt = 1;
            bool test = testing.testFunc();

            Console.Clear();


            //get board length and height
            rowLength = boardSize.GetLength(0);
            colLength = boardSize.GetLength(1);

#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            StartUI();
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed

            //set whole board to spaces
            for (int i = 0; i < rowLength; i++)
            {
                for (int j = 0; j < colLength; j++)
                {
                    boardSize[i, j] = 1;
                }
                Console.Write(Environment.NewLine);

            }
            for (int i = 0; i < rowLength; i++)
            {
                for (int j = 0; j < colLength; j++)
                {
                    dummyBoard[i, j] = 1;
                }
                Console.Write(Environment.NewLine);

            }

            //test glider
            boardSize[20, 5] = 0;
            boardSize[20, 6] = 0;
            boardSize[20, 7] = 0;
            boardSize[21, 7] = 0;
            boardSize[22, 6] = 0;

            //ask user for initial positions

            if (consoleUI)
            {
                initPrint();
            }
            else
            {
                Console.Clear();
            }

            //initialize timer
            generation = new System.Timers.Timer(interval);
            generation.Elapsed += OnTimedEvent;
            generation.AutoReset = true;
            generation.Enabled = true;


            while (true)
            {

            }


        }

        private void initPrint()
        {
            //initial board print
            Console.Clear();
            for (int i = 0; i < (rowLength * 2) + 2; i++)
            {
                Console.Write("x");
            }
            Console.WriteLine();
            for (int i = 0; i < rowLength; i++)
            {
                Console.Write("x");
                for (int j = 0; j < colLength; j++)
                {
                    Console.Write(boardSize[i, j] + 1);
                }
                Console.Write("x");
                Console.Write(Environment.NewLine);

            }
            for (int i = 0; i < (rowLength * 2) + 2; i++)
            {
                Console.Write("x");
            }
            Console.WriteLine();
        }
        public async Task StartUI()
        {
            await Task.Run(() =>
            {
                gameWindow window = new gameWindow();
                window.parent = this;
                window.Show();
            });
        }
        private void printBoard()
        {
            
            //print out game board
            for (int i = 0; i < (rowLength * 2) + 2; i++)
            {
                Console.Write("x");
            }
            Console.WriteLine();
            for (int i = 0; i < rowLength; i++)
            {
                Console.Write("x");
                for (int j = 0; j < colLength; j++)
                {
                    Console.Write(boardSize[i, j] + 1);
                }
                Console.Write("x");
                Console.Write(Environment.NewLine);

            }
            for (int i = 0; i < (rowLength * 2) + 2; i++)
            {
                Console.Write("x");
            }
            Console.WriteLine();
            Console.Write("Generation: " + generationNum + "     " + Convert.ToUInt32(colLength));
        }

        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            generationNum++;
            //boardSize[generationNum, 0] = 0;
            //boardSize[generationNum + 1, 0] = 1;
            Console.Clear();

            //do da game of LIFE
            for (int i = 0; i < rowLength; i++)
            {
                for (int j = 0; j < colLength; j++)
                {
                    gameOfLife(i, j);
                }
            }

            //dummyBoard = boardSize.Clone() as string[,];
            for (int i = 0; i < rowLength; i++)
            {
                for (int j = 0; j < colLength; j++)
                {
                    boardSize[i, j] = dummyBoard[i, j];
                }

            }
            displayBoard = boardSize.Clone() as int[,];
            if (consoleUI)
            {
                printBoard();
            }
            
            
        }
        
        private void gameOfLife(int y, int x)
        {
            //check if each surrounding cell is alve or not (1 = dead, 0 = alive)
            surroundingPop = 0;
            if (x != 0 & y != 0 & x != rowLength - 1 & y != rowLength - 1 & x != colLength - 1 & y != colLength - 1)
            {
                if (boardSize[y - 1, x] == 0)
                {
                    surroundingPop++;
                }
                if (boardSize[y + 1, x] == 0)
                {
                    surroundingPop++;
                }
                if (boardSize[y, x - 1] == 0)
                {
                    surroundingPop++;
                }
                if (boardSize[y, x + 1] == 0)
                {
                    surroundingPop++;
                }
                if (boardSize[y + 1, x + 1] == 0)
                {
                    surroundingPop++;
                }
                if (boardSize[y - 1, x - 1] == 0)
                {
                    surroundingPop++;
                }
                if (boardSize[y + 1, x - 1] == 0)
                {
                    surroundingPop++;
                }
                if (boardSize[y - 1, x + 1] == 0)
                {
                    surroundingPop++;
                }
            }

            //check if cell is alive or dead
            bool isAlive;
            if (boardSize[y, x] == 1)
            {
                isAlive = false;
            }
            else
            {
                isAlive = true;
            }

            //put the rules in place
            if (isAlive)
            {
                if (surroundingPop < 2 | surroundingPop > 3)
                {
                    if (dummyBoard[y, x] != 1)
                    {
                        dummyBoard[y, x] = 1;
                    }
                }
                else
                {
                    if (dummyBoard[y, x] != 0)
                    {
                        dummyBoard[y, x] = 0;
                    }
                }
            }
            else 
            {
                if (surroundingPop == 3)
                {
                    if (dummyBoard[y, x] != 0)
                    {
                        dummyBoard[y, x] = 0;
                    }
                }
            }
        }
    }

    class gameWindow
    {
        public Program parent;
        public void Show()
        {
            
            VideoMode mode = new VideoMode(Convert.ToUInt32(parent.rowLength * 10), Convert.ToUInt32(parent.colLength * 10));
            
            RenderWindow window = new RenderWindow(mode, "CSharp's game of life", Styles.Close);

            window.Closed += (obj, e) =>
            {
                window.Close();
            };
            window.KeyPressed += (sender, e) =>
            {
                Window windows = (Window)sender;
                if (e.Code == Keyboard.Key.Escape)
                {
                    windows.Close();
                    Environment.Exit(0);
                }
            };
            
            RectangleShape[,] cells = new RectangleShape[parent.rowLength, parent.colLength];
            for (int i = 0; i < parent.rowLength; i++)
            {
                for (int j = 0; j < parent.colLength; j++)
                {
                    cells[i, j] = new RectangleShape();
                    cells[i, j].Size = new Vector2f(10, 10);
                    cells[i, j].Origin = new Vector2f(0, 0);
                    cells[i, j].Position = new Vector2f(i * 10, j * 10);
                    cells[i, j].FillColor = Color.Black;
                }
            }
            Font font = new Font("arial.ttf");
            Text text = new Text("csharp is best lang", font);
            Text rowLength = new Text("Generation: " + parent.generationNum.ToString(), font);
            text.CharacterSize = 40;
            rowLength.CharacterSize = mode.Height / 50;
            float tWidth = text.GetLocalBounds().Width;
            float tHeight = text.GetLocalBounds().Height;
            float txOff = text.GetLocalBounds().Left;
            float tyOff = text.GetLocalBounds().Top;
            float rWidth = rowLength.GetLocalBounds().Width;
            float rHeight = rowLength.GetLocalBounds().Height;
            float rxOff = rowLength.GetLocalBounds().Left;
            float ryOff = rowLength.GetLocalBounds().Top;
            text.Origin = new Vector2f(tWidth / 2f + txOff, tHeight / 2 + tyOff);
            text.Position = new Vector2f(window.Size.X / 2f, window.Size.Y / 2f);
            rowLength.Origin = new Vector2f(0, 0);
            rowLength.Position = new Vector2f(0, 0);

            Clock clock = new Clock();
            float deltaTime = 0f;
            float angle = 0f;
            float angleSpeed = 90f;

            while (window.IsOpen)
            {
                deltaTime = clock.Restart().AsSeconds();
                rowLength = new Text("Generation: " + parent.generationNum.ToString(), font);
                rowLength.CharacterSize = mode.Height / 50;
                angle += angleSpeed * deltaTime;
                window.DispatchEvents();
                window.Clear();
                text.Rotation = angle;
                window.Draw(text);
                for (int i = 0; i < parent.rowLength; i++)
                {
                    for (int j = 0; j < parent.colLength; j++)
                    {
                        if (parent.displayBoard[i, j] == 1)
                        {
                            if (cells[i, j].FillColor != Color.Black)
                            {
                                cells[i, j].FillColor = Color.Black;
                            }
                        }
                        else if (parent.displayBoard[i, j] == 0)
                        {
                            if (cells[i, j].FillColor != Color.White)
                            {
                                cells[i, j].FillColor = Color.White;
                            }
                        }
                    }
                }
                for (int i = 0; i < parent.rowLength; i++)
                {
                    for (int j = 0; j < parent.colLength; j++)
                    {
                        window.Draw(cells[i, j]);
                    }
                }
                window.Draw(rowLength);
                window.Display();
                
            }
        }
    }
}
